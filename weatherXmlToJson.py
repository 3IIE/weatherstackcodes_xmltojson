#!/usr/bin/env python3
""" Program to convert or print weather xml to json. 
        Requires bs4 and lxml parser installed
        Example with all flags:

            python3 weatherXmlToJson.py -i 4 -p -h -w\n"""
from sys import argv as sArgs
from json import dumps as jDumps
from bs4 import BeautifulSoup as BS

class xmlToJson():
    """ This class reads the XML file wwoConditionCodes.xml prints the xml 
    as json or writes the json to a file in the same directory. """
    def __init__(self, *args):
        """ Help options: 
    -h           Show help menu.
    --help

 Indentation of the json file:
    -i           Indent the json file to number following
    --indent     the flag if number is given.

 Print json to screen:
    -p           Show json data.
    --print

 Write json data to file in same directory as xml file:
    -w           Writes json file.
    --write"""

        indnt = self.jsonIndent(args)
        self.json = jDumps(self.xmlWeatherToJson(), indent=indnt)

        if True in [True for x in ['-w', '--write'] if x in args]:
            self.writeJson(self.json)

        if True in [True for x in ['-p', '--print'] if x in args]:
            print(self.json)

        if True in [True for x in ['-h', '--help'] if x in args]:
            print(self.__init__.__doc__)

    def getSoup(self):
        """ Return parseable xml as BeautifulSoup. """
        with open('wwoConditionCodes.xml', 'r') as fle: 
            return BS(fle.read(), "lxml") 

    def xmlWeatherToJson(self):
        """ Return BeautifulSoup as dict() """
        soup = self.getSoup().find_all('condition')
        a = {x: {} for x in range(len(soup))}
        [a[w].update({y.name: y.text.strip()}) for w, x in enumerate(soup) for y in x if y != '\n']
        return [a[x] for x in a]

    def writeJson(self, dta):
        """ Write the json file to the same directory as the parsed xml. """
        with open('wwoConditionCodes.json', 'w') as fle:
            fle.write(dta)

    def jsonIndent(self, args):
        """ Creates the json indentation. """
        indnt = [x for x in ['-i', '--index'] if x in args]
        if indnt != []:
            try:
                i = args[args.index(indnt[0]) + 1]
                i = int(i) if str(i).isdigit() else i
            except (IndexError, ValueError):
                indnt = None
            else:
                i = i if isinstance(i, int) else None
                indnt = i if i is None or i >= 0 else None
        else:
            indnt = 4
        return indnt

if __name__ == '__main__':
    xmlToJson(*sArgs[1:])
